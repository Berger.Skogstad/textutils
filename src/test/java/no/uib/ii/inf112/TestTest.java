package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class TestTest {
	TextAligner aligner = new TextAligner() {

		public String center(String text, int width) {
			int extra = (width - text.length()) / 2;
			return " ".repeat(extra) + text + " ".repeat(extra);
		}

		public String flushRight(String text, int width) {
			return String.format("%1$" + width + "s", text);
		}

		public String flushLeft(String text, int width) {
			return String.format("%1$" + (-width) + "s", text);
		}

		public String justify(String text, int width) {
			String out = "";
			String[] li = text.split(" ");
			int spaces = (width - text.length()) / (li.length - 1);
			for (int i = 0; i < li.length - 1; i++) {
				System.out.println(spaces);
				out += flushLeft(li[i], spaces - 1);
			}
			return out + li[li.length - 1];
		}};
		
	@Test
	void test() {
		assertEquals("  A  ", aligner.center("A", 5));
	}

	@Test
	void testCenter() {
		assertEquals("  A  ", aligner.center("A", 5));
		assertEquals(" foo ", aligner.center("foo", 5));

	}
	@Test
	void testLeft() {
		assertEquals("A    ", aligner.flushLeft("A", 5));
	}
	@Test
	void testRight() {
		assertEquals("    A", aligner.flushRight("A", 5));
	}
	@Test
	void testJustify() {
		assertEquals("fee   fie   foo", aligner.justify("fee fie foo", 15));
	}
}
